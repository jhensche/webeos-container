# webeos-container

This project consists of the docker base image (with all the required packages installed) for WebEOS on top of CERN Centos 7 docker base image.

## OKD4-Install automatic update

Based on a schedule the new release of image is built and pushed to registery and a MR is opened against [OKD4-install](https://gitlab.cern.ch/paas-tools/okd4-install) repo with the latest image tag.
Below is a list of environment variables that the update process requires:

- BRANCH_NAME - name of new branch created in the update process in OkD4-install repository

- COMMIT_USER - email of user who commits the updation of tags in OKD4-install

- MR_ASSIGNEES - MR Reviewers on OKD4-install rep MR

- OKD4_INSTALL_REPO_PATH - namespace and repository name of the OKD4-install repository

- OKD4_INSTALL_JOB_NAME - name of job in the new MR pipeline to run after successfully opening the MR

Details of a new or existing Project Access token of [OKD4-install](https://gitlab.cern.ch/paas-tools/okd4-install) repo is to be provided for the update process to be able to access it.

- UPDATE_BOT_USERNAME - name of project access token

- UPDATE_BOT_AUTH_TOKEN - project access token
