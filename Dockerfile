FROM cern/cc7-base

# This environment variable will be used by liveness/readiness probes to keep track of Apache configuration changes
ENV RELOAD_TIMESTAMP_FILE=/etc/httpd/conf/last_reload

# CERNOnly repo provides the Oracle client, TNSnames.ora and related packages for PHP
COPY conf_files/yum-repos/ /etc/yum.repos.d/

# Added perl packages #L56-61, w.r.t Issue https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/77
RUN yum -y update && \
    yum -y install yum-plugin-priorities && \
    yum -y install apr \
                   apr-util \
                   httpd \
                   mod_ssl \
                   cyrus-sasl-lib \
                   expat \
                   db4 \
                   which \
                   unzip \
                   zip \
                   libstdc++ \
                   ncurses \
                   postgresql-libs \
                   sqlite \
                   php \
                   php-xml \
                   php-gd \
                   php-mbstring \
                   php-pgsql \
                   php-odbc \
                   php-ldap \
                   php-imap \
                   php-pdo \
                   php-mysql \
                   php-pear \
                   php-devel \
                   php-bcmath \
                   php-dba \
                   php-pear-DB \
                   php-pecl-ssh2 \
                   php-mcrypt \
                   php-soap \
                   perl-LDAP \
                   perl-IO-Socket-SSL \
                   perl-Net-SSLeay \
                   perl-GD \
                   mod_perl \
                   perl-Digest-HMAC \
                   perl-Digest-SHA1 \
                   perl-IO-String \
                   perl-libxml-perl \
                   perl-MailTools \
                   perl-MIME-Lite \
                   perl-Parse-RecDescent \
                   perl-SQL-Statement \
                   perl-TermReadKey \
                   perl-Text-Template \
                   perl-version \
                   perl-XML-Dumper \
                   perl-XML-Grove \
                   perl-XML-RegExp \
                   perl-CGI \
                   perl-YAML \
                   perl-WWW-Curl \
                   perl-SOAP-Lite \
                   perl-XML-Simple \
                   perl-Net-IP \
                   perl-DBI \
                   perl-Tie-DBI \
                   perl-DBD-MySQL \
                   perl-DBD-Oracle \
                   python-flup \
                   pysvn \
                   python-matplotlib \
                   pycairo \
                   python-dateutil \
                   pytz \
                   MySQL-python \
                   python-ldap \
                   python-lxml \
                   python-pip \
                   python3 \
                   log4shib \
                   libcurl-openssl \
                   mod_auth_kerb \
                   gettext \
                   numpy \
                   cvs \
                   giflib \
                   mysql \
                   gd \
                   libc-client \
                   libxslt \
                   libX11 \
                   libXpm \
                   fontconfig \
                   freetype \
                   libpng \
                   pkgconfig \
                   autoconf \
                   automake \
                   imake \
                   libXdmcp \
                   libXau \
                   gnuplot \
                   rlog \
                   dbus-python \
                   lynx \
                   rcs \
                   mc \
                   highlight \
                   ghostscript \
                   ghostscript-fonts \
                   libXfont \
                   libXt \
                   libfontenc \
                   ttmkfdir \
                   xorg-x11-font-utils \
                   urw-fonts \
                   ImageMagick \
                   alsa-lib \
                   audiofile \
                   avahi \
                   avahi-glib \
                   gamin \
                   gnome-keyring \
                   gnome-vfs2 \
                   libart_lgpl \
                   libbonobo \
                   libbonoboui \
                   libglade2 \
                   libgnome \
                   libgnomecanvas \
                   libgnomeui \
                   libgsf \
                   librsvg2 \
                   libwmf \
                   shared-mime-info \
                   dialog \
                   netpbm \
                   netpbm-progs \
                   psutils \
                   cronolog \
                   libffi \
                   iftop \
                   elinks \
                   perl-Date-Manip \
                   glibc-devel \
                   openssl098e \
                   xmltooling-schemas \
                   opensaml-schemas \
                   perl-Spreadsheet-XLSX \
                   perl-Unicode-String \
                   xapian-core \
                   xapian-core-libs \
                   xapian-bindings \
                   xapian-bindings-python \
                   xapian-bindings-ruby \
                   tcl-xapian \
                   pandoc \
                   oracle-instantclient-basic \
                   oracle-instantclient-sqlplus \
                   oracle-instantclient-tnsnames.ora \
                   cx_Oracle \
                   php-oci8 \
                   ld-linux.so.2 \
                   mod_auth_openidc \
                   kstart \
                   krb5-workstation && \
    yum clean all -y

# Due to https://gitlab.com/gitlab-org/gitlab-runner/issues/1736 we must make sure to `chmod`
# all the files we COPY with the desired mode, or they will be world-writable by default.
COPY conf_files/error_pages/* /var/www/html/
COPY conf_files/css /var/www/html/css/
COPY conf_files/robots/* /var/www/html/

# - Make sure `/etc/httpd` is only readable by root since we need to store OIDC secrets in conf files.
# - Files in /var/www/html and /etc/yum.repos.d/ should be world-readable (error pages)
# - Initialize last_reload timestamp file
RUN chmod -R 700 /etc/httpd && \
    chmod -R 755 /var/www/html /etc/yum.repos.d/ && \
    touch ${RELOAD_TIMESTAMP_FILE}

EXPOSE 8080

CMD [ "httpd", "-DFOREGROUND" ]
